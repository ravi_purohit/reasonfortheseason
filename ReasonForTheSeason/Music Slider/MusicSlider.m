//
//  MusicSlider.m
//
//  Created by isol08 on 20/07/16.
//  Copyright (c) 2016 ISOL. All rights reserved.
//

#import "MusicSlider.h"

@implementation MusicSlider
@synthesize maximumValue;
@synthesize minimumValue;
@synthesize value;

float mpadding;
float mwhiteDoteWidth;
float mwhiteDoteHeight;
float mbgTrackwidth;
float mlastValue;
float mposition;
float mtrackHeight;
float mthumbWidth;
float mthumbHeight;
BOOL mslide;

UIImageView *mthumbImage,*mwhiteDote1,*mwhiteDote5,*mwhiteDote15,*mwhiteDote50,*mtrackImg;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    value=0;
    maximumValue=100;
    minimumValue=1;
    mlastValue=0;
    mpadding=10;
    mwhiteDoteWidth=7;
    mwhiteDoteHeight=7;
    mtrackHeight=8;
    mthumbWidth=15;
    mthumbHeight=25;
    mbgTrackwidth=frame.size.width-(2*mpadding);
    
    NSLog(@"frame width %f",frame.size.width);
    if(self=[super initWithFrame:CGRectMake(0,0,frame.size.width, frame.size.height)])
    {
//        self.backgroundColor = [UIColor yellowColor];
        
        mtrackImg = [[UIImageView alloc]initWithFrame:CGRectMake(mpadding,(self.frame.size.height/2)-(mtrackHeight/2),mbgTrackwidth,mtrackHeight)];
        [mtrackImg setImage:[UIImage imageNamed:@"bar.png"]];
        mtrackImg.userInteractionEnabled=NO;
        [self addSubview:mtrackImg];
    
        
        mthumbImage=[[UIImageView alloc]initWithFrame:CGRectMake([self valueToXpos],(self.frame.size.height/2)-(mthumbHeight/2),mthumbWidth,mthumbHeight)];
        [mthumbImage setImage:[UIImage imageNamed:@"pointer.png"]];
        mthumbImage.userInteractionEnabled=NO;
        [self addSubview:mthumbImage];
        
    }
    return self;
}


#pragma mark - Supportive methods

                                                                             
-(float)valueToXpos
{
    return (mbgTrackwidth/maximumValue)*value;
}
-(float)xposToValue:(float)pos
{
    return (pos/mbgTrackwidth)*maximumValue;
}



-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
      return YES;
}


-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {

    CGPoint touchPoint = [touch locationInView:self];
    if((mtrackImg.frame.origin.x<=touchPoint.x)&&(touchPoint.x<mtrackImg.frame.origin.x+mbgTrackwidth))
    {
        mposition=touchPoint.x;
        mthumbImage.center = CGPointMake(mposition, mthumbImage.center.y);
        mslide=YES;
        
    }
    [self setThumbImage];

}


-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
   CGPoint touchPoint = [touch locationInView:self];
   if((mtrackImg.frame.origin.x<=touchPoint.x)&&(touchPoint.x<mtrackImg.frame.origin.x+mbgTrackwidth))
    {
        mposition=touchPoint.x;
        mthumbImage.center = CGPointMake(mposition, mthumbImage.center.y);
        mslide=YES;
        
    }
    return YES;
    
}

-(void)setThumbImage {
    
    mthumbImage.center = CGPointMake(mposition,mthumbImage.center.y);
    mlastValue=mposition;
    NSLog(@"mposition is %f",mposition);
    NSLog(@"mbgTrackwidth is %f",mbgTrackwidth);
    value  = (mposition-mpadding)/mbgTrackwidth;
    NSLog(@"value is %f",value);
    [_delegate musicDidChange:value];
    
}

-(void)setValue:(float)value
{
    mposition = value*mbgTrackwidth+mpadding;
    [self setThumbImage];
}


@end
