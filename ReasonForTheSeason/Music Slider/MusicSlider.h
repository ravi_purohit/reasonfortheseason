//
//  MusicSlider.h
//
//  Created by isol08 on 20/07/16.
//  Copyright (c) 2016 ISOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MusicSliderDelegate <NSObject>

@required

- (void)musicDidChange:(float)value;

@end

@interface MusicSlider : UIControl


@property (nonatomic, assign) id <MusicSliderDelegate> delegate;
@property(nonatomic) float minimumValue;
@property(nonatomic) float maximumValue;
@property(nonatomic) float value;

-(void)setValue:(float)value;

@end
