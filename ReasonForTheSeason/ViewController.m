//
//  ViewController.m
//  ReasonForTheSeason
//
//  Created by User on 29/11/17.
//  Copyright © 2017 User. All rights reserved.
//

#import "ViewController.h"
#import "UIImage+animatedGIF.h"
#import "PageCollectionViewCell.h"
#import "LastCollectionViewCell.h"
#import "FirstCollectionViewCell.h"


@interface ViewController ()

@end

@implementation ViewController

@synthesize bgLeftImageView,bgRightImageView,toonImageView,musicSlider,voiceSlider,lblContent,collectionV,nextButton,previousButton,playPauseButton,voicelblImage;

CustomSlider *vocSlider;
MusicSlider *musSlider;
AVAudioPlayer *audioPlayer,*screenAudioPlayer;
NSInteger currentPage,totalPage,lastPageIndex;

NSArray *imageGifAry,*lblContentAry;
CGPoint _lastContentOffset;
BOOL directionRight=YES,isPlaying=YES,userManualyPaused=NO;
NSAttributedString *page0,*page1,*page2,*page3,*page4,*page5,*page6,*page7,*lastPageContent;
NSURL *screenAudioFileLocationURL;
CGRect screenBounds;
float voiceValue;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    totalPage = 9;
    currentPage= 0;
    previousButton.enabled = NO;
    playPauseButton.hidden = YES;
    voicelblImage.hidden = YES;
    voiceSlider.hidden = YES;
    imageGifAry = [[NSArray alloc]initWithObjects:@"snowman_face",@"snowman_wave",@"tree",@"santa",@"presents",@"snowman_face",@"snowman_cover",@"baby", nil];
    
    screenBounds = [[UIScreen mainScreen] bounds];
    if(screenBounds.size.width<screenBounds.size.height)
    {
        screenBounds = CGRectMake(0, 0, screenBounds.size.height, screenBounds.size.width);
    }
    
    NSLog(@"screen widht is this :%f",screenBounds.size.width);
    NSLog(@"screen Height is this :%f",screenBounds.size.height);
    
    // For iPhone X resolution Design
    if(screenBounds.size.width >= 812)
    {
        _playPauseButTop.constant = 10;
        _playPauseButLead.constant = 10;
        _homeLead.constant = 10;
        _homeBottom.constant = 10;
    }
    
    // Background Gif Images
    NSURL *urlbgLeft = [[NSBundle mainBundle] URLForResource:@"snowfall_left" withExtension:@"gif"];
    bgLeftImageView.image = [UIImage animatedImageWithAnimatedGIFURL:urlbgLeft];
    NSURL *urlbgRight = [[NSBundle mainBundle] URLForResource:@"snowfall_right" withExtension:@"gif"];
    bgRightImageView.image = [UIImage animatedImageWithAnimatedGIFURL:urlbgRight];
    collectionV.contentOffset = CGPointZero;;      
    
    [self playTheBackgroundAudio];
//    [self performSelector:@selector(playTheScreenAudio) withObject:nil afterDelay:2];
    [self prepareAttributedText];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [musicSlider setNeedsLayout];
    [voiceSlider setNeedsLayout];
    [musicSlider layoutIfNeeded];
    [voiceSlider layoutIfNeeded];
    [self setSliderView];
}


#pragma mark - supportive methods


-(void)prepareAttributedText{
    
    page0 = [[NSAttributedString alloc]initWithString:@""];
    
    UIFont *font = [UIFont fontWithName:@"Architects Daughter" size:8.0];
    NSDictionary *attribDict = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *oneblankLine = [[NSAttributedString alloc]initWithString:@"\n" attributes:attribDict];
    
    NSMutableAttributedString *temp = [[NSMutableAttributedString alloc]initWithString:@"Hello my name is Thomas -\nI have a question, yes I do!\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"What's the reason for the season?\nI should know and so should you!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"What is Christmas all about?\nHumm...I wonder, let me see...\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"I know what it is,\nits all about the Tree!"]];
    
    page1 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"Garland on the branches...\nand all those little lights!\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Ornaments and colors...\nshining oh so bright!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"The reason for the season...\nit's all about the tree!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"No that's not it at all,\nit simply cannot be."]];
    
    page2 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"Oh I know what it is -\n it's SANTA and his sleigh...\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"with all the reindeer flying -\nwhile Rudolph lights the way!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"No that's not it at all -\neven though he's big and jolly...\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Santa's not the reason, oh my gosh \nand oh my golly!"]];

    page3 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"Oh I know what it is -\nit must be all the TOYS!\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"The presents that are left\nfor all the girls and boys!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Wrapped in shiny paper -\nwith ribbons and a bow...\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"The reason for the season is the gifts\n now don't you know!"]];
    
    page4 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"No that's not it at all -\neven though they bring us joy...\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Christmas isn't Christmas\njust because we all get toys.\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"What's the reason for the season -\nfancy parties, yummy food?\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Brand new outfits, falling snow -\neverybody's happy mood?"]];
    
    page5 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"No that's not it at all -\nit's something so much more.\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"It's not the Tree or Santa\nor Gifts bought in a store.\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Christmas is a birthday -\nand a very special one...\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"It's the birth of baby Jesus -\n God's One and only son!"]];
    
    page6 = temp;
    
    temp = [[NSMutableAttributedString alloc]initWithString:@"So remember every Christmas,\nas you open all your toys -\n"];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"God's gift of baby Jesus\nis the reason for real joy!\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Thank you, God, for Jesus -\nhe's the reason for this day...\n"]];
    [temp appendAttributedString:oneblankLine];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Bless you, bless me, bless everyone -\nin Jesus' name we pray."]];
    
    page7 = temp;
    
    lblContentAry = [[NSArray alloc]initWithObjects:page0,page1,page2,page3,page4,page5,page6,page7, nil];
    
    
    UIFont *font1 = [UIFont fontWithName:@"Juice ITC" size:32.0];
    NSDictionary *attribDict1 = [NSDictionary dictionaryWithObject:font1 forKey:NSFontAttributeName];
    NSAttributedString *onebigblankLine = [[NSAttributedString alloc]initWithString:@"\n" attributes:attribDict1];
    
    UIFont *font111 = [UIFont fontWithName:@"Juice ITC" size:30.0];
    NSMutableDictionary *attribDict111 = [NSMutableDictionary dictionaryWithObject:font111 forKey:NSFontAttributeName];
    [attribDict111 setObject:[NSNumber numberWithInteger:10] forKey:NSBaselineOffsetAttributeName];
    
    UIFont *lowfont = [UIFont fontWithName:@"Juice ITC" size:2.0];
    NSDictionary *lowfontDict = [NSDictionary dictionaryWithObject:lowfont forKey:NSFontAttributeName];

    NSDictionary *atDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:0] forKey:NSBaselineOffsetAttributeName];
     NSAttributedString *superStar = [[NSAttributedString alloc]initWithString:@"*" attributes:atDict];
    temp = [[NSMutableAttributedString alloc]init];
    [temp appendAttributedString:superStar];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Visit our Website"]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"www.iknowthereasonfortheseason.com"]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    [temp appendAttributedString:onebigblankLine];
    [temp appendAttributedString:superStar];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Noah's Ark - A Story About Faith"]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n" attributes:lowfontDict]];
    NSDictionary *atDict1 = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:-2] forKey:NSBaselineOffsetAttributeName];
    NSAttributedString *superStar1 = [[NSAttributedString alloc]initWithString:@"*" attributes:atDict1];
    [temp appendAttributedString:superStar1];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Coming in Spring 2018" attributes:attribDict111]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    [temp appendAttributedString:onebigblankLine];
    [temp appendAttributedString:superStar];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Android and PC capabilities in 2018"]];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    [temp appendAttributedString:oneblankLine];
    UIFont *font11 = [UIFont fontWithName:@"Helvetica Neue" size:13.0];
    NSDictionary *attribDict11 = [NSDictionary dictionaryWithObject:font11 forKey:NSFontAttributeName];
    
    NSMutableDictionary *atDict111 = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInteger:0] forKey:NSBaselineOffsetAttributeName];
    [atDict111 setObject:font11 forKey:NSFontAttributeName];
    NSAttributedString *superStar111 = [[NSAttributedString alloc]initWithString:@"*" attributes:atDict111];
    [temp appendAttributedString:superStar111];
    [temp appendAttributedString:[[NSAttributedString alloc]initWithString:@"Copyright 2017\nAll illustrations and content are the property of the author\nand cannot be reproduced or sold without written consent from the author." attributes:attribDict11]];
    
    lastPageContent = temp;
}


#pragma mark- Audio Handling Methods


-(void)playTheBackgroundAudio
{
    NSURL *audioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"christmas-magic" withExtension:@"wav"];

    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileLocationURL error:&error];
    [audioPlayer setNumberOfLoops:-1];

    if (error) {
        NSLog(@"%@", [error localizedDescription]);

    } else
    {
        //Load the audio into memory
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        [audioPlayer setVolume:0.15];
    }
}

-(void)playTheScreenAudio
{
    NSURL *audioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page1" withExtension:@"mp3"];
    
    NSError *error;
    screenAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileLocationURL error:&error];
    screenAudioPlayer.delegate = self;
    [screenAudioPlayer setNumberOfLoops:0];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        
    } else
    {
        //Load the audio into memory
        [screenAudioPlayer prepareToPlay];
        [screenAudioPlayer play];
        voiceValue = 0.5;
        [screenAudioPlayer setVolume:0.5];
    }
}


-(void)changeTheAudioAfterDelay {
    
    [screenAudioPlayer stop];
    screenAudioPlayer = nil;
    if(currentPage ==1)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page1" withExtension:@"mp3"];
    }
    else if(currentPage ==2)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page2" withExtension:@"mp3"];
    }
    else if(currentPage ==3)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page3" withExtension:@"mp3"];
    }
    else if(currentPage ==4)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page4" withExtension:@"mp3"];
    }
    else if(currentPage ==5)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page5" withExtension:@"mp3"];
    }
    else if(currentPage ==6)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page6" withExtension:@"mp3"];
    }
    else if(currentPage ==7)
    {
        screenAudioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"Page7" withExtension:@"mp3"];
    }
    else
    {
        screenAudioFileLocationURL = nil;
    }
    
    [self performSelector:@selector(ChangeAudioTrackAccordingtoTheCurrentPage) withObject:nil afterDelay:0];
}


-(void)ChangeAudioTrackAccordingtoTheCurrentPage {
    
    NSError *error;
    screenAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:screenAudioFileLocationURL error:&error];
    screenAudioPlayer.delegate = self;
    [screenAudioPlayer setNumberOfLoops:0];
    
    if (error) {
        NSLog(@"screen audio error %@", [error localizedDescription]);
        
    }
    else
    {
        //Load the audio into memory
        [screenAudioPlayer prepareToPlay];
        if(isPlaying)
        {
            [screenAudioPlayer play];
            [self setButtonIfPlayed:YES];
        }
        else
        {
            if(userManualyPaused)
            {
                [screenAudioPlayer pause];
                [self setButtonIfPlayed:NO];
            }
            else
            {
                [screenAudioPlayer play];
                [self setButtonIfPlayed:YES];
            }
        }
        [screenAudioPlayer setVolume:voiceValue];
    }
}


-(void)setButtonIfPlayed:(BOOL)isPlayed {
    
    if(isPlayed)
    {
      isPlaying = YES;
      [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    }
    else
    {
       isPlaying = NO;
       [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    }
}



- (void)voiceDidChange:(float)value {
    
    NSLog(@"value of the voice slider is this %f",value);
    if(value<0.05)
    {
        value = 0.0;
    }
    voiceValue = value;
    [screenAudioPlayer setVolume:value];
}


- (void)musicDidChange:(float)value {
    
    NSLog(@"value of the music slider is this %f",value);
    
    if(value<0.05)
    {
        value = 0.0;
    }
    
    value = value * (0.60);
    [audioPlayer setVolume:value];
}




#pragma  mark  AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag
{
    if(flag)
    {
        NSLog(@"audio did finish successfully");
        isPlaying = NO;
        [screenAudioPlayer pause];
        [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"audio did finish Failed");
    }
}


#pragma mark upperBluePartComponents


-(void)setSliderView{
    
    vocSlider = [[CustomSlider alloc]initWithFrame:voiceSlider.frame];
    vocSlider.delegate = self;
    [vocSlider setValue:0.5];
    [voiceSlider addSubview:vocSlider];
    musSlider=  [[MusicSlider alloc]initWithFrame:musicSlider.frame];
    musSlider.delegate = self;
    [musSlider setValue:0.15];
    [musicSlider addSubview:musSlider];
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark collection veiw Delegate and Datasource method


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return totalPage;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *ccell;
    
   if(indexPath.row == 0) // First Cell of the Collection view
   {
       FirstCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FirstCollectionViewCell" forIndexPath:indexPath];
       NSString*imageNamed = [imageGifAry objectAtIndex:indexPath.row];
       
       //       NSLog(@"image name is this :%@",imageNamed);
       UIImage *image = [UIImage imageNamed:imageNamed];
       cell.imgView.image = image;
       
       if(currentPage == indexPath.row)
       {
           if(screenBounds.size.width > 480)
           {
               [self performSelector:@selector(callAfterDelay:) withObject:indexPath afterDelay:0.30];
           }
       }
       if(screenBounds.size.width >= 812)
       {
           cell.imgViewLeading.constant = 10;
           cell.viewTrailing.constant = 43;
       }
       ccell = cell;
   }
   else if(indexPath.row < totalPage-1) // Mid Cells of the Collection view
   {
       PageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PageCollectionViewCell" forIndexPath:indexPath];
    
       NSString*imageNamed = [imageGifAry objectAtIndex:indexPath.row];
    
//       NSLog(@"image name is this :%@",imageNamed);
       UIImage *image = [UIImage imageNamed:imageNamed];
       cell.imgView.image = image;

       if(currentPage == indexPath.row)
       {
           if(screenBounds.size.width > 480)
           {
              [self performSelector:@selector(callAfterDelay:) withObject:indexPath afterDelay:0.30];
           }
       }
        cell.lblContent.attributedText = [lblContentAry objectAtIndex:indexPath.row];
       if(screenBounds.size.width >= 812)
       {
           cell.imgViewLeading.constant  = 30;
           cell.lblContentTrailing.constant = 30;
       }
       ccell = cell;
   }
   else   // Last Cell of the Collection view
   {
       LastCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LastCell" forIndexPath:indexPath];
       cell.lblContent.attributedText = lastPageContent;
       ccell = cell;
   }

   return ccell;
    
}


-(void)callAfterDelay:(NSIndexPath*)indexPath {
    
  if(currentPage == indexPath.row)
  {
     NSString *nameGifFile = [NSString stringWithFormat:@"g%@",[imageGifAry objectAtIndex:currentPage]];
     NSLog(@"nameGifFile name is this :%@",nameGifFile);
     NSURL *urlToon = [[NSBundle mainBundle] URLForResource:nameGifFile withExtension:@"gif"];
    
       
        dispatch_async(dispatch_get_main_queue(), ^{
            if(currentPage == indexPath.row)
            {
               PageCollectionViewCell *cell = (PageCollectionViewCell *)[collectionV cellForItemAtIndexPath:[NSIndexPath indexPathForItem:currentPage inSection:0]];
               cell.imgView.image = [UIImage animatedImageWithAnimatedGIFURL:urlToon];
            }
            
        });
  }
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat cellWidth =collectionView.frame.size.width;
    CGFloat cellHeight=collectionView.frame.size.height;
        return CGSizeMake(cellWidth,cellHeight);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if(screenBounds.size.width >= 812) // For iPhone X only
    {
        
        return UIEdgeInsetsMake(0, -43, 0, 0); // top, left, bottom, right
    }
    else
    {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }

}

#pragma mark - scroll view delegates


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    _lastContentOffset = scrollView.contentOffset;
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (_lastContentOffset.x < (int)scrollView.contentOffset.x) {
        // NSLog(@"Scrolled Right");
        directionRight=YES;
        
    }
    else if (_lastContentOffset.x > (int)scrollView.contentOffset.x) {
        
        // NSLog(@"Scrolled left");
        directionRight=NO;
    }
    
}


- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(directionRight)
    {
        if(currentPage < (totalPage-1))
        {
            currentPage++;
            [self enableDisableNextPreviousButtons];
        }
    }
    else
    {
        if(currentPage > 0)
        {
            currentPage--;
            [self enableDisableNextPreviousButtons];
        }
    }
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGPoint point = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    CGPoint locationPoint = [collectionV convertPoint:point fromView:self.view];

    NSLog(@"locationPoint x value is this %f",locationPoint.x);
    
    
    if((0<locationPoint.x)&&(locationPoint.x < screenBounds.size.width))
    {
        currentPage = 0;
    }
    else if((screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 2*screenBounds.size.width))
    {
        currentPage = 1;
    }
    else if((2*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 3*screenBounds.size.width))
    {
        currentPage = 2;
    }
    else if((3*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 4*screenBounds.size.width))
    {
        currentPage = 3;
    }
    else if((4*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 5*screenBounds.size.width))
    {
        currentPage = 4;
    }
    else if((5*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 6*screenBounds.size.width))
    {
        currentPage = 5;
    }
    else if((6*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 7*screenBounds.size.width))
    {
        currentPage = 6;
    }
    else if((7*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 8*screenBounds.size.width))
    {
        currentPage = 7;
    }
    else if((8*screenBounds.size.width<locationPoint.x)&&(locationPoint.x < 9*screenBounds.size.width))
    {
        currentPage = 8;
    }
    
   if(lastPageIndex != currentPage)
   {
       NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentPage inSection:0];
       [collectionV reloadItemsAtIndexPaths:@[indexPath]];
       [self performSelector:@selector(changeTheAudioAfterDelay) withObject:nil afterDelay:0];
       lastPageIndex = currentPage;
   }
   [self enableDisableNextPreviousButtons];
}



#pragma mark - Button Click Event Handling Methods




- (IBAction)previousButtonClicked:(id)sender {
    
 if(currentPage > 0)
 {
     currentPage--;
     NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentPage inSection:0];
     [collectionV reloadItemsAtIndexPaths:@[indexPath]];
     [collectionV scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];

     [self performSelector:@selector(changeTheAudioAfterDelay) withObject:nil afterDelay:0];
     [self enableDisableNextPreviousButtons];
     lastPageIndex = currentPage;
//     NSLog(@"current page is on previous %d",currentPage);
 }
    
}

- (IBAction)nextButtonClicked:(id)sender {

    
    if(currentPage < (totalPage-1))
    {
        currentPage++;
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentPage inSection:0];
        [collectionV reloadItemsAtIndexPaths:@[indexPath]];
        [collectionV scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [self performSelector:@selector(changeTheAudioAfterDelay) withObject:nil afterDelay:0];
        [self enableDisableNextPreviousButtons];
        lastPageIndex = currentPage;
//        NSLog(@"current page is on next %d",currentPage);
    }
}

- (IBAction)homeButtonClicked:(id)sender {
    
    if(currentPage !=0)
    {
        currentPage = 0;
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentPage inSection:0];
        [collectionV reloadItemsAtIndexPaths:@[indexPath]];
        [collectionV scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [self performSelector:@selector(changeTheAudioAfterDelay) withObject:nil afterDelay:0];
        [self enableDisableNextPreviousButtons];
    }
    
}


- (IBAction)playPauseButtonClicked:(id)sender {
    
    if(isPlaying)
    {
        NSLog(@"is paused");
        isPlaying = NO;
        userManualyPaused = YES;
        [screenAudioPlayer pause];
        [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"is playing");
        isPlaying = YES;
        userManualyPaused = NO;
        [screenAudioPlayer play];
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    }
    
}


-(void)enableDisableNextPreviousButtons {
    
    if(currentPage == (totalPage-1))
    {
        nextButton.enabled = NO;
        playPauseButton.hidden = YES;
        voiceSlider.hidden = YES;
        voicelblImage.hidden = YES;
        previousButton.enabled = YES;
    }
    else
    {
        nextButton.enabled = YES;
        if(currentPage == 0)
        {
            previousButton.enabled = NO;
            playPauseButton.hidden = YES;
            voiceSlider.hidden = YES;
            voicelblImage.hidden = YES;
        }
        else
        {
            previousButton.enabled = YES;
            playPauseButton.hidden = NO;
            voiceSlider.hidden = NO;
            voicelblImage.hidden = NO;
        }

    }

}

@end
