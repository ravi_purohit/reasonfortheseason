//
//  CustomSlider.m
//
//  Created by isol08 on 20/07/16.
//  Copyright (c) 2016 ISOL. All rights reserved.
//

#import "CustomSlider.h"

@implementation CustomSlider
@synthesize maximumValue;
@synthesize minimumValue;
@synthesize value;

float padding;
float whiteDoteWidth;
float whiteDoteHeight;
float bgTrackwidth;
float lastValue;
float position;
float trackHeight;
float thumbWidth;
float thumbHeight;
BOOL slide;

UIImageView *thumbImage,*whiteDote1,*whiteDote5,*whiteDote15,*whiteDote50,*trackImg;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    value=0;
    maximumValue=100;
    minimumValue=1;
    lastValue=0;
    padding=10;
    whiteDoteWidth=7;
    whiteDoteHeight=7;
    trackHeight=8;
    thumbWidth=15;
    thumbHeight=25;
    bgTrackwidth=frame.size.width-(2*padding);
    
    NSLog(@"frame width %f",frame.size.width);
    if(self=[super initWithFrame:CGRectMake(0,0,frame.size.width, frame.size.height)])
    {
//        self.backgroundColor = [UIColor yellowColor];
        
        trackImg = [[UIImageView alloc]initWithFrame:CGRectMake(padding,(self.frame.size.height/2)-(trackHeight/2),bgTrackwidth,trackHeight)];
        [trackImg setImage:[UIImage imageNamed:@"bar.png"]];
        trackImg.userInteractionEnabled=NO;
        [self addSubview:trackImg];
    
        
        thumbImage=[[UIImageView alloc]initWithFrame:CGRectMake([self valueToXpos],(self.frame.size.height/2)-(thumbHeight/2),thumbWidth,thumbHeight)];
        [thumbImage setImage:[UIImage imageNamed:@"pointer.png"]];
        thumbImage.userInteractionEnabled=NO;
        [self addSubview:thumbImage];
        
    }
    return self;
}

#pragma mark - Supportive methods
                                                                             
-(float)valueToXpos
{
    return (bgTrackwidth/maximumValue)*value;
}
-(float)xposToValue:(float)pos
{
    return (pos/bgTrackwidth)*maximumValue;
}



-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
      return YES;
}


-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint touchPoint = [touch locationInView:self];
    if((trackImg.frame.origin.x<=touchPoint.x)&&(touchPoint.x<trackImg.frame.origin.x+bgTrackwidth))
    {
        position=touchPoint.x;
        thumbImage.center = CGPointMake(position, thumbImage.center.y);
        slide=YES;
        
    }
    [self setThumbImage];
}


-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
   CGPoint touchPoint = [touch locationInView:self];
   if((trackImg.frame.origin.x<=touchPoint.x)&&(touchPoint.x<trackImg.frame.origin.x+bgTrackwidth))
    {
        position=touchPoint.x;
        thumbImage.center = CGPointMake(position, thumbImage.center.y);
        slide=YES;
        
    }
    return YES;
    
}

-(void)setThumbImage
{
    thumbImage.center = CGPointMake(position,thumbImage.center.y);
    lastValue=position;
    
    value  = (position-padding)/bgTrackwidth;
    NSLog(@"value is %f",value);
    [_delegate voiceDidChange:value];
}

-(void)setValue:(float)value
{
    position = value*bgTrackwidth+padding;
    [self setThumbImage];
}


@end
