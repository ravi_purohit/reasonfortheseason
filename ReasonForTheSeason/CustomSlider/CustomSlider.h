//
//  CustomSlider.h
//
//  Created by isol08 on 20/07/16.
//  Copyright (c) 2016 ISOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomSliderDelegate <NSObject>

@required
- (void)voiceDidChange:(float)value;

@end


@interface CustomSlider : UIControl

@property (nonatomic, assign) id <CustomSliderDelegate> delegate;

@property(nonatomic) float minimumValue;
@property(nonatomic) float maximumValue;
@property(nonatomic) float value;

-(void)setValue:(float)value;

@end
