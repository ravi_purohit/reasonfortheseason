//
//  ViewController.h
//  ReasonForTheSeason
//
//  Created by User on 29/11/17.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSlider.h"
#import "MusicSlider.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController <CustomSliderDelegate,MusicSliderDelegate,AVAudioPlayerDelegate,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *bgLeftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bgRightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *toonImageView;
@property (weak, nonatomic) IBOutlet UIView *musicSlider;
@property (weak, nonatomic) IBOutlet UIView *voiceSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionV;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playPauseButLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playPauseButTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeBottom;

@property (weak, nonatomic) IBOutlet UIImageView *voicelblImage;



- (IBAction)previousButtonClicked:(id)sender;

- (IBAction)nextButtonClicked:(id)sender;

- (IBAction)homeButtonClicked:(id)sender;
- (IBAction)playPauseButtonClicked:(id)sender;







@end

