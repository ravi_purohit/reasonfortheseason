//
//  LastCollectionViewCell.h
//  ReasonForTheSeason
//
//  Created by User on 06/12/17.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LastCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end
