//
//  PageCollectionViewCell.h
//  ReasonForTheSeason
//
//  Created by User on 04/12/17.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblContentTrailing;


@end
