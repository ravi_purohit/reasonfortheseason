//
//  FirstCollectionViewCell.h
//  ReasonForTheSeason
//
//  Created by User on 14/12/17.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UIImageView *rightViewAboveImg;
@property (weak, nonatomic) IBOutlet UILabel *lblcopyRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTrailing;


@end
